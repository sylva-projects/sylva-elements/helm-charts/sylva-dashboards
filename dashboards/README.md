Flux dashboards are based on the following example:

https://github.com/fluxcd/flux2-monitoring-example/tree/main/monitoring/configs/dashboards

Some modifications were made in order to be compliant with our exposed metrics for Flux resources.

Loki dashboard is based on the following example:

https://grafana.com/grafana/dashboards/18042-logging-dashboard-via-loki-v2/

Crossplane dashboard is based on the following example:

https://grafana.com/grafana/dashboards/21169-crossplane/
